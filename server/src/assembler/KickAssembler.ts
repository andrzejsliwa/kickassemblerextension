/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
  
    KickAssembler.ts
  
    Provides a Mechanism for Source File Validation using Kick Assembler
  
*/


let tmp = require('tmp');

import Logging from "../utils/Logging";
const log = Logging.log();

import PathUtils from "../utils/PathUtils";
import StringUtils from "../utils/StringUtils";
import LineUtils from "../utils/LineUtils";

import { ISettings } from "../providers/SettingsProvider";
import { TextDocument } from "vscode-languageserver/lib/main";
import { AssemblerInfo } from "./AssemblerInfo"
import { writeFileSync, unlinkSync,	readFileSync } from 'fs';

import {
	spawnSync,
} from 'child_process';
import { IParameter } from "../definitions/LanguageDefinition";
import SourceUtils from "../utils/SourceUtils";

/**
 * A Special Type of Field that descries a Range of characters in a Source File
 */
export interface ISourceRange {
    startLine:number;
    startPosition:number;
    endLine:number;
    endPosition:number;
    fileIndex:number;
}

/**
 * A Library represents an external module that provides Functions for use in the Assembler
 */
export interface ILibrary {
    name:string;
    entry:string;
    data:string;
}

/**
 * Directives instruct the Assembler to do Something
 */
export interface IDirective {
    name:string;
    example:string;
    description:string;
}

/**
 * Preprocessor Directives control what is included in the source before the Main Assembly is started
 */
export interface IPreprocessorDirective {
    name:string;
    example:string;
    description:string;
}

/**
 * Represents a File included in the Source File
 */
export interface IFile {
    index:number;
    system:boolean;
    uri:string;
}

/**
 * Represents the Syntax on a Specific Line of Code in the Source File
 */
export interface ISyntax {
    type:string;
    sourceRange:ISourceRange
    line:number;
    scope:number;
}

/**
 * Represents an Error returning from the Assembler
 */
export interface IError {
	level: string;
	range: ISourceRange;
	message: string;
}

/**
 * A Collection of Information returning from the Assembler
 */
export interface IAssemblerInfo {
    libraries:ILibrary[];
    directives:IDirective[];
    preprocessorDirectives:IPreprocessorDirective[];
    files:IFile[];
    syntax:ISyntax[];
    errors:IError[];
}

export interface ILine {
    number:number;
    scope:number;
    scopeName:string;
    text:string;
    file:IFile;
}

export interface ISourceFile {
    //  file object
    file:IFile;
    //  lines in the sourcefile
    lines:ILine[];
    //  is external from the editor source, something from an #include
    isExternal:boolean;
}

/**
 * Results from the Assembler that will be useful for other Developer functions
 */
export interface IAssemblerResults {
    assemblerInfo:IAssemblerInfo;
    sourceFiles:ISourceFile[];
    stdout:string;
    stderr:string;
    status:number;
}

/**
 *  The purpose of this Module is to Provide a mechanism for
 *  assembling a source file, and using the information returned
 *  from the assembler for other functions in the server
 *  extension.
 * 
 */
export class KickAssembler {

    constructor() {
        log.trace('[KickAssembler]');
    }

    public assemble(settings:ISettings, document:TextDocument, processIncludes:boolean|false):IAssemblerResults {
        log.trace('[KickAssembler] assemble');

        if (!settings.valid) {
            console.warn("Invalid Settings - Cannot Assemble");
            return {
                assemblerInfo: null,
                sourceFiles: null,
                status: 99,
                stderr: null,
                stdout: null
            }
        }

        //  run the assembler and return the info data
        var assemblerResults = this.assembleSource(document.uri, document.getText(), settings);

        //  add the scoped source files
        assemblerResults.sourceFiles = [];

        for (let file of assemblerResults.assemblerInfo.files) {

            let sourceFile = <ISourceFile>{};
            sourceFile.file = file;
            sourceFile.file.uri = file.uri;
            sourceFile.file.system = false;

            if (file.uri.toLocaleLowerCase().indexOf("autoinclude.asm") > 0) {
                sourceFile.file.system = true;
                sourceFile.isExternal = true;
            } else {
                var data = "";
                if (file.uri.toLocaleLowerCase().indexOf("tmp-") > 0) {
                    data = document.getText();
                    sourceFile.isExternal = false;
                } else {
                    var data = readFileSync(file.uri, 'utf8');
                    sourceFile.isExternal = true;
                }
                //sourceFile.lines = this.createLines(data, file);
                var filex:IFile;
                sourceFile.lines = SourceUtils.getLines(StringUtils.splitIntoLines(data), filex);

            }

            assemblerResults.sourceFiles.push(sourceFile); 
        }

        return assemblerResults; 
    }

    private assembleSource(uri:string, source:string, settings:ISettings):IAssemblerResults {
        log.trace('[KickAssembler] assembleSource');

        if (!settings.valid) {
            console.warn("Invalid Settings - Cannot Assemble");
            return {
                assemblerInfo: null,
                sourceFiles: null,
                status: 99,
                stderr: null,
                stdout: null
            }
        }

        //  determine path of file
        var pathname = PathUtils.getPathFromFilename(PathUtils.uriToPlatformPath(uri));
        
        //  build temp tables for assembly and info
   		var tmpSource = tmp.tmpNameSync({ dir: pathname, keep: true });
		var asmInfo = tmp.tmpNameSync({ dir: pathname, keep: true });
        writeFileSync(tmpSource, source);
        
        //  run kickass
   		let java = spawnSync(settings.javaPath, [
               "-jar", 
               settings.assemblerPath, 
               tmpSource, 
               '-noeval', 
               '-warningsoff', 
               '-asminfo', 
               'all', 
               '-asminfofile', 
               asmInfo], { cwd: pathname });


        log.debug('java return status = ' + java.status);

        //  return asminfo
        var data = readFileSync(asmInfo, 'utf8');

        //  remove work files
        unlinkSync(tmpSource);
        unlinkSync(asmInfo);

        var assemblerResults = <IAssemblerResults>{};

        //  get assembler info
        assemblerResults.assemblerInfo = this.parseAssemblerInfo(data);

        //  return assembler results
        assemblerResults.stdout = java.stdout.toString();
        assemblerResults.stderr = java.stderr.toString();
        assemblerResults.status = java.status;

        return assemblerResults;
    }

    private parseAssemblerInfo(asmInfo:string):IAssemblerInfo {

        var assemblerInfo = new AssemblerInfo(asmInfo);
        return assemblerInfo;
    }

    private createLines(source:string, file:IFile):ILine[] {

        let lines = [];
        let sourceLines = StringUtils.splitIntoLines(source);
        let next = 0;
        let last = [];
        let scope = 0;
        let scopeName = "";

        if (source) {
            
            for (var i = 0; i < sourceLines.length; i++) {

                var sourceLine = LineUtils.removeComments(sourceLines[i]); 

                let line = <ILine>{};
                line.number = i;
                line.scope = scope;
                line.scopeName = scopeName;
                line.text = sourceLines[i];
                line.file = file;

                if (sourceLine) {

                    //	search for { - add to scope
                    if (sourceLine.indexOf("{") >= 0) {
                        last.push(scope);
                        next += 1;
                        scope = next;
                    }
                    
                    //	search for } - remove from scope
                    if (sourceLine.indexOf("}") >= 0) {
                        scope = last.pop();
                    }
                }

                lines.push(line);
            }
        }
        return lines;
    }

}