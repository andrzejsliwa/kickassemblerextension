/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import {
	Diagnostic,
	DiagnosticSeverity,
	IConnection,
} from "vscode-languageserver";

import { IProjectInfoProvider, Provider } from "./Provider";
import { IAssemblerResults } from "../assembler/KickAssembler";

/*
	Provides Diagnostic Information for the Client

	This Provider is currently only used for Returning Error 
	information, back to the Client. The Assembler Results already
	contains the errors, and source locations.

	In the future, this Provider may be used to send other information.
*/
export default class DiagnosticsProvider extends Provider {

	constructor(connection:IConnection, projectInfoProvider:IProjectInfoProvider) {
		log.trace('[DiagnosticProvider]');
		super(connection, projectInfoProvider);
	}

	public process(uri:string):void {
		log.trace('[DiagnosticProvider] process');

		//	for return
		const diagnostics:Diagnostic[] = [];
		
		//	grab the assembler results from the last change
		const results:IAssemblerResults = this.getProjectInfo().getAssemblerResults();
		
		//	create diagnostic for each error on the last compile
		if (results.status > 0) {

			//	only send back diagnostics for the
			//	file that is being worked on
			var fileNumber = -1;

			for (let file of results.assemblerInfo.files) {
				if (file.uri.toLocaleLowerCase().indexOf("tmp-") > 0) {
					fileNumber = file.index;
				} 
			}			

			if (results.assemblerInfo.errors.length > 0) {
				results.assemblerInfo.errors.forEach((error) => {
					if (error.range.fileIndex == fileNumber) {
						diagnostics.push( {
							severity: DiagnosticSeverity.Error,
							range: {
								start: { line: error.range.startLine, character: error.range.startPosition },
								end: { line: error.range.endLine, character: error.range.endPosition},
							},
							message: error.message,
							source: "kickassembler",
						});
					}
				});
			}
		}

		//	return the diagnostic information
		this.getConnection().sendDiagnostics({ uri, diagnostics });
	}

}
