/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

export default class Logging {

    private static _logger:any;

    public static log():any {
        if (this._logger == null) {
            var log4js = require('log4js');

            log4js.configure({
                levels: {
                    NOTICE: { value: 20001, colour: 'green' },
                    FINE: { value: 1, colour: 'grey' },
                },
                appenders: { out: { type: 'stdout' } },
                categories: { default: { appenders: ['out'], level: 'trace' } }
            });

            this._logger = log4js.getLogger();
            this._logger.info('[Logger] logging started');
            this._logger.fine(log4js.levels);
            this._logger.trace(`- log level is ${this._logger.level}`);

        }
        return this._logger;
    }
}